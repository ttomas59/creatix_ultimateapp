$(document).ready(function () {

    var minWidth = 1440;
    if($(window).outerWidth() > minWidth){
        $('.beta-app').tooltip({trigger: 'manual'}).tooltip('show');
    }

    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1){
        $('.carousel-arrow').css('top',300);
    }


    $('.carousel').carousel();

    $('.grid').masonry({
        itemSelector: '.grid-item',
        percentPosition: true
    });

    var gradient = "rgba(114,109,187,1) 0%, rgba(71,209,195,1) 100%"; //green gradient
    var target = document.getElementsByClassName('app-img-temlate');
    $.each(target, function( index, value ) {
        GradientMaps.applyGradientMap(value, gradient);
    });

    var logging = false;

    $("#nav-login-form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var emailElement = $("#nav-login-form input[name='email']");
        var submitButton = $("#nav-login-form button[type=submit]");
        formProcess(logging,form,emailElement, submitButton);
    });

    $("#action-form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var emailElement = $("#action-form input[name='email']");
        var submitButton = $("#action-form button[type=submit]");
        formProcess(logging,form,emailElement, submitButton);
    });

    $("#subscribe-form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var emailElement = $("#subscribe-form input[name='email']");
        var submitButton = $("#subscribe-form button[type=submit]");
        formProcess(logging,form,emailElement, submitButton);
    });

    var middleWidth = 768;

    $('.nav-searching').hover(function (e) {
        $('.searching-box').animate({width: '250px'},500);
        if($(window).outerWidth() < middleWidth){
            var margin = Math.round(($(window).outerWidth()-250)/2);
            $('.searching-box').css('margin-left',margin);
        }

    },function() {
        $('.searching-box').css('width','');
        $('.searching-box').css('position','absolute');
        $('.searching-box').css('margin-left','');
    });

    $('.searching-close').click(function () {
        $('.searching-placeholder').val('');
    });


   resetDefault();

});

var redColor = '#de265f';

function formProcess(processing,form, emailElement, submitButton){

    if(!processing){
        var values = {};
        $.each(form.serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

        if(!validateEmail(values.email)){
            emailElement.tooltip({
                trigger: 'manual'
            });
            emailElement.attr('title', 'Email address is invalid!').tooltip('fixTitle').tooltip('show');
            emailElement.css('color',redColor);
            return;
        }
        submitButton.attr("disabled", true);
        processing = true;
        submitButton.html('Logging <i class="fa fa-spinner fa-spin"></i>');

        setTimeout(function () {
            processing = false;
            submitButton.attr("disabled", false);
            if(isUsedEmail(values.email)){
                emailElement.tooltip({
                    trigger: 'manual'
                });
                emailElement.attr('title', 'This email is used!').tooltip('fixTitle').tooltip('show');
                emailElement.css('color',redColor);
                submitButton.css('color',redColor);
                submitButton.html('Error!');
            } else {
                submitButton.html('Success');
            }
        },2000);
    }
}

function resetDefault() {
    $("#nav-login-form input[name='email']").keydown(function () {
        $("#nav-login-form input[name='email']").tooltip('hide');
        $("#nav-login-form input[name='email']").css('color','rgb(51, 51, 51)');
        $("#nav-login-form button[type=submit]").css('color','white');
        $("#nav-login-form button[type=submit]").html('LOGIN');
    });

    $("#action-form input[name='email']").keydown(function () {
        $("#action-form input[name='email']").tooltip('hide');
        $("#action-form input[name='email']").css('color','rgb(51, 51, 51)');
        $("#action-form button[type=submit]").css('color','white');
        $("#action-form button[type=submit]").html('GO');
    });

    $("#subscribe-form input[name='email']").keydown(function () {
        $("#subscribe-form input[name='email']").tooltip('hide');
        $("#subscribe-form input[name='email']").css('color','rgb(51, 51, 51)');
        $("#subscribe-form button[type=submit]").css('color','white');
        $("#subscribe-form button[type=submit]").html('GO');
    });
}

function isUsedEmail(email) {
    var usedEmail = 'test@test.com';
    return usedEmail === email;
}

function validateEmail(email){
    var emailReg = new RegExp(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/);
    return emailReg.test(email);
}